//禁止右键
//$(document).bind("contextmenu",function(){return false;});
//禁止选择
// $(document).bind("selectstart",function(){return false;});
//禁止F12
// document.onkeydown = function(){
//     if(window.event && window.event.keyCode == 123) {
//         event.keyCode = 0;
//         event.returnValue = false;
//     }
// }
$(".show_img").click(function () {
    var url = $(this).attr('src') || '';
    if (url == '') return false;
    layer.open({
        type: 1,
        title: '查看图片（点击图片可旋转）',
        area: ['800px', '640px'],
        shadeClose: true,
        content: '<div class="show_img_click" style="line-height: 580px;text-align: center">' +
            '<img src="' + url + '" style="max-width: 800px;max-height: 560px;cursor: pointer" title="点击旋转">' +
            '</div>',
        success: function () {
            var current = 0;
            $(document).on("click", ".show_img_click", function () {
                //图片旋转
                current = (current + 90) % 360;
                $(this).find('img').css('transform', 'rotate(' + current + 'deg)');
            });
        }
    });
});
function pdf_preview(obj) {
    var full_url = $(obj).attr('data-value') || '';
    if (full_url == '') return false;
    layer.open({
        title: '文件预览',
        type: 2,
        shade: 0.2,
        maxmin: true,
        shadeClose: true,
        area: ['90%', '90%'],
        content: full_url,
        btn: ['取消'],
        btnAlign: 'c',
        yes: function (index, layero) {
            layer.close(index); //关闭弹层
        }
    });
}

